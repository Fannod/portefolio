const i18nConfig = {
    locales: ["fr","en"],
    defaultLocale: "fr",
    prefixDefault: "false"
}

module.exports = i18nConfig